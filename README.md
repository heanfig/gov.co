# Prueba de conocimientos Angular, Bootstrap

Prueba de Valoracion

![preview](https://i.imgur.com/S6KBY4o.png)

________

# Previsualizacion:

https://gov-co.vercel.app/

# Tecnologías usadas

* Angular 7
* Bootstrap 4
* MDBootstrap Angular
* NgRx
* Firebase
* Implementación de Pruebas unitarias
* Schema.org para mejor SEO
* Cookies para el manejo de notificaciones top
* PWA
* SASS
* 
# Mas importantes Características

* NgRx manejo de estado
* Actualizaciones en Tiempo Real usando "Firebase real time database"
* Responsive design

# Secciones Implementadas

1. Encabezado

    Hace Uso de cookies para visualizar la alerta

   ![preview](https://i.imgur.com/6obcLob.png)

2. Menu

   Hace un autoScroll en la seccion a al cual desea visualizar, en cada uno de los items

   ![preview](https://i.imgur.com/VgpcPwE.png)

3. Tramites Mas usados (llave en el json **diligencies**)

    Contiene la lista de tramites, ambos estan reactivos y consumen la misma datos

    Si al json de configuracion de tramite se le configura la opcion **featured** automaticamente queda visible en el componente de cards (del lado izquierdo)

   ![preview](https://i.imgur.com/Lp9LBFb.png)

4. Queremos conocer tu opinion
   
   Carga la lista de opiniones, ambos estan reactivos y consumen desde la base de datos

   La cantidad de participantes, se consume automaticamente desde el formulario es decir, si la persona agrega un comentario el contador aumentara reactivamente

   ![preview](https://i.imgur.com/BgqpTLG.png)

   El formulario muestra solo el comentario que tenga la llave en el json **featured** solo mostrará uno independientemente si hayan 2 valores **featured**

5. Informate  
   
   Carga la lista de noticias, ambos estan reactivos y consumen desde la base de datos, las imagenes estan hosteadas usando Firebase Storage, pero se invoca solo el cdn
   
6. Otros temas de Interes  
   
   Carga la lista de noticias, ambos estan reactivos y consumen desde la base de datos, las imagenes estan hosteadas usando Firebase Storage, pero se invoca solo el cdn
   

# Instalación:

**Instalación:**
npm i

**Correr Proyecto:**
ng serve -o

**Pruebas Unitarias:**
ng test

**Compilación para Producción:**
ng build --prod

**Configuración Para otro proyecto de Firebase:**
https://firebase.google.com/docs/web/setup
# Base de datos

La base de datos esta en firebase, sin embargo la instantanea de la base de datos esta en la raiz del proyecto, un archivo llamado <code>database.json</code> se importa el json en la raiz de la base de datos de firebase que vaya a configurar

![preview](https://i.imgur.com/BDts6IL.png)

# Estructura de directorios:

    > src/app/core
    Modulo de Componentes Base del proyecto tales como header y footer

    > src/app/home
    Modulo de Componentes De la home

    > src/reducers
    Reductores de la Aplicacion

    > src/app/shared
    Modulo de Componentes compartidos
    Posdata: En este modulo estan los componentes de listas de los diferentes componentes, se colocaron aca porque estos componentes pueden ser reusados por otro modulo, si se deseara expandir

# Useful Links:

# Visita mi perfil : https://github.com/heanfig
