// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCBml7NVMblZCk849cA9kJodrzahBMMSSk",
    authDomain: "govco-de473.firebaseapp.com",
    databaseURL: "https://govco-de473.firebaseio.com",
    projectId: "govco-de473",
    storageBucket: "govco-de473.appspot.com",
    messagingSenderId: "384015435142",
    appId: "1:384015435142:web:883c399818322a1bb1d807",
    measurementId: "G-WX810YM78E"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
