import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AngularFireDatabase } from '@angular/fire/database';
import { YourOpinion } from '../models/your-opinion.model';

@Injectable({
  providedIn: 'root',
})
export class YourOpinionService {
  
  // get firebase url Database from environment
  url = environment.firebase.databaseURL;

  /**
   * Creates an instance of YourOpinionService.
   * @param {AngularFireDatabase} db
   * @memberof YourOpinionService
   */
  constructor(private db: AngularFireDatabase) { }
  
  /**
   * Add Participation to Current Firebase Node
   * @param {string} participation
   * @param {YourOpinion} youropinion
   * @memberof YourOpinionService
   */
  add(opinion: string, youropinion: YourOpinion) {
    const participation = this.db.list(`youropinion/${youropinion.key}/participations/`);
    return participation.push(opinion);
  }

  /**
   * get List of opinions from firebase
   * @returns
   * @memberof YourOpinionService
   */
  get() {
    return this.db.list(`youropinion/`).snapshotChanges();
  }

}
