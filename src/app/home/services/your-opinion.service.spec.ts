import { TestBed, inject } from '@angular/core/testing';

import { YourOpinionService } from './your-opinion.service';

describe('YourOpinionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YourOpinionService]
    });
  });

  it('should be created', inject([YourOpinionService], (service: YourOpinionService) => {
    expect(service).toBeTruthy();
  }));
});
