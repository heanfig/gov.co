import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root',
})
export class InformYourselfService {
  url = environment.firebase.databaseURL;

  constructor(private db: AngularFireDatabase) { }

  get() {
    return this.db.list(`inform-yourself/`).snapshotChanges();
  }

}
