import { TestBed, inject } from '@angular/core/testing';

import { OtherLinksService } from './other-links.service';

describe('OtherLinksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OtherLinksService]
    });
  });

  it('should be created', inject([OtherLinksService], (service: OtherLinksService) => {
    expect(service).toBeTruthy();
  }));
});
