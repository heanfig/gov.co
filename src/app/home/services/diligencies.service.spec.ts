import { TestBed, inject } from '@angular/core/testing';

import { DiligenciesService } from './diligencies.service';

describe('ProjectsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiligenciesService]
    });
  });

  it('should be created', inject([DiligenciesService], (service: DiligenciesService) => {
    expect(service).toBeTruthy();
  }));
});
