import { TestBed, inject } from '@angular/core/testing';

import { InformYourselfService } from './inform-yourself.service';

describe('InformYourselfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InformYourselfService]
    });
  });

  it('should be created', inject([InformYourselfService], (service: InformYourselfService) => {
    expect(service).toBeTruthy();
  }));
});
