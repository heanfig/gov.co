export interface InformYourself {
  key?: any;
  date?: Date;
  title?: string;
  imageUrl?: string;
  size?: string;
}
