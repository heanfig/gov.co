export interface OtherLink {
  key?: any;
  title?: string;
  imageUrl?: string;
  callToActionText?: string;
  excerpt?: string;
  link?: string;
}
