export interface YourOpinion {
  key?: any;
  state?: string;
  title?: string;
  author?: string;
  description?: string;
  featured?: boolean;
  participations?: string[];
}
