export interface Diligency {
  key?: any;
  title?: string;
  author?: string;
  availableOnLine?: boolean;
  diligencyWithCost?: boolean;
  featured: boolean;
}
