import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformYourselfSectionComponent } from './inform-yourself-section.component';

describe('InformYourselfSectionComponent', () => {
  let component: InformYourselfSectionComponent;
  let fixture: ComponentFixture<InformYourselfSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformYourselfSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformYourselfSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
