import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from 'src/app/reducers';
import { InformYourself } from '../../models/inform-yourself.model';
import * as fromInformYourself from '../../store/inform-yourself/inform-yourself.actions';
import { getInformYourself, getAllLoaded } from '../../store/inform-yourself/inform-yourself.selectors';

@Component({
  selector: 'app-inform-yourself-section',
  templateUrl: './inform-yourself-section.component.html',
  styleUrls: ['./inform-yourself-section.component.scss']
})
export class InformYourselfSectionComponent implements OnInit {

  informYourself$: Observable<InformYourself[] | null>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.isLoading$ = this.store.select(getAllLoaded);
    this.informYourself$ = this.store.pipe(
      select(getInformYourself),
      map( (informYourself: InformYourself[]) => {
        if (!informYourself) {
          this.store.dispatch(new fromInformYourself.InformYourselfQuery());
        }
        return informYourself;
      })
    );
  }

}
