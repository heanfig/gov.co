import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiligenciesSectionComponent } from './diligencies-section.component';

describe('DiligenciesSectionComponent', () => {
  let component: DiligenciesSectionComponent;
  let fixture: ComponentFixture<DiligenciesSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiligenciesSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiligenciesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
