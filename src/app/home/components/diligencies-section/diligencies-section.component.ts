import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { getDiligencies, getAllLoaded } from '../../store/diligencies/diligencies.selectors';
import { Diligency } from '../../models/diligency.model';
import { AppState } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import * as fromDiligencies from '../../store/diligencies/diligencies.actions';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-diligencies-section',
  templateUrl: './diligencies-section.component.html',
  styleUrls: ['./diligencies-section.component.scss']
})
export class DiligenciesSectionComponent implements OnInit {
  
  diligencies$: Observable<Diligency[] | null>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.isLoading$ = this.store.select(getAllLoaded);
    this.diligencies$ = this.store.pipe(
      select(getDiligencies),
      map( (diligencies: Diligency[]) => {
        if (!diligencies) {
          this.store.dispatch(new fromDiligencies.DiligenciesQuery());
        }
        return diligencies;
      })
    );
  }

}
