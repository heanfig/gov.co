import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { YourOpinion } from '../../models/your-opinion.model';
import { getYourOpinion, getAllLoaded } from '../../store/your-opinion/your-opinion.selectors';
import * as fromYourOpinion from '../../store/your-opinion/your-opinion.actions';
import { AppState } from 'src/app/reducers';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'app-your-opinion-section',
  templateUrl: './your-opinion-section.component.html',
  styleUrls: ['./your-opinion-section.component.scss']
})
export class YourOpinionSectionComponent implements OnInit {
  
  yourOpinion$: Observable<YourOpinion[] | null>;
  isLoading$: Observable<boolean>;

  /**
   *Creates an instance of YourOpinionSectionComponent.
   * @param {Store<AppState>} store
   * @memberof YourOpinionSectionComponent
   */
  constructor(
    private store: Store<AppState>,
    private toaster: Toaster
  ) { }

  /**
   * ngInit method of application
   * @memberof YourOpinionSectionComponent
   */
  ngOnInit() {
    this.isLoading$ = this.store.select(getAllLoaded);
    this.yourOpinion$ = this.store.pipe(
      select(getYourOpinion),
      map( (diligencies: YourOpinion[]) => {
        if (!diligencies) {
          this.store.dispatch(new fromYourOpinion.YourOpinionQuery());
        }
        return diligencies;
      })
    );
  }

  /**
   * handler submit event on partipation box field 
   * @memberof YourOpinionSectionComponent
   */
  onSubmittedOpinion(data: any): void {
    this.store.dispatch(new fromYourOpinion.YourOpinionAdded({ 
      opinion: data.opinion, 
      yourOpinion: data.yourOpinion 
    }));
    this.toaster.open({
      position: 'top-right',
      text: 'Comentario Enviado',
      caption: 'Gracias',
      type: 'success',
    });
  }

}
