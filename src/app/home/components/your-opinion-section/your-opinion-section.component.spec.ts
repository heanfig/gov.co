import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourOpinionSectionComponent } from './your-opinion-section.component';

describe('YourOpinionSectionComponent', () => {
  let component: YourOpinionSectionComponent;
  let fixture: ComponentFixture<YourOpinionSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourOpinionSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourOpinionSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
