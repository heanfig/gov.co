import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppState } from 'src/app/reducers';
import { OtherLink } from '../../models/other-links.model';
import * as fromOtherLinks from '../../store/other-links/other-links.actions';
import { getOtherLinks, getAllLoaded } from '../../store/other-links/other-links.selectors';

@Component({
  selector: 'app-other-links-section',
  templateUrl: './other-links-section.component.html',
  styleUrls: ['./other-links-section.component.scss']
})
export class OtherLinksSectionComponent implements OnInit {

  otherLinks$: Observable<OtherLink[] | null>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.isLoading$ = this.store.select(getAllLoaded);
    this.otherLinks$ = this.store.pipe(
      select(getOtherLinks),
      map( (otherLinks: OtherLink[]) => {
        if (!otherLinks) {
          this.store.dispatch(new fromOtherLinks.OtherLinksQuery());
        }
        return otherLinks;
      })
    );
  }

}
