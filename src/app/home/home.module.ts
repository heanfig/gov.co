import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './containers/home.component';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { ButtonsModule, InputsModule, CardsModule, WavesModule, IconsModule, ModalModule } from 'angular-bootstrap-md';
import { ToastNotificationsModule } from 'ngx-toast-notifications';
import * as fromDiligencies from './store/diligencies/diligencies.reducer';
import * as fromYourOpinion from './store/your-opinion/your-opinion.reducer';
import * as fromOtherLinks from './store/other-links/other-links.reducer';
import * as fromInformYourself from './store/inform-yourself/inform-yourself.reducer';
import { EffectsModule } from '@ngrx/effects';
import { DiligenciesEffects } from './store/diligencies/diligencies.effects';
import { YourOpinionEffects } from './store/your-opinion/your-opinion.effects';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DiligenciesSectionComponent } from './components/diligencies-section/diligencies-section.component';
import { InformYourselfSectionComponent } from './components/inform-yourself-section/inform-yourself-section.component';
import { OtherLinksSectionComponent } from './components/other-links-section/other-links-section.component';
import { YourOpinionSectionComponent } from './components/your-opinion-section/your-opinion-section.component';
import { YourOpinionService } from './services/your-opinion.service';
import { DiligenciesService } from './services/diligencies.service';
import { InformYourselfService } from './services/inform-yourself.service';
import { OtherLinksService } from './services/other-links.service';
import { InformYourselfEffects } from './store/inform-yourself/inform-yourself.effects';
import { OtherLinksEffects } from './store/other-links/other-links.effects';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    SharedModule,
    HomeRoutingModule,
    HttpClientModule,
    FormsModule,
    ButtonsModule,
    InputsModule,
    WavesModule,
    IconsModule,
    CardsModule,
    StoreModule.forFeature('diligencies', fromDiligencies.diligenciesReducer),
    EffectsModule.forFeature([DiligenciesEffects]),
    StoreModule.forFeature('yourOpinion', fromYourOpinion.yourOpinionReducer),
    EffectsModule.forFeature([YourOpinionEffects]),
    StoreModule.forFeature('informYourself', fromInformYourself.informYourselfReducer),
    EffectsModule.forFeature([InformYourselfEffects]),
    StoreModule.forFeature('otherLinks', fromOtherLinks.otherLinksReducer),
    EffectsModule.forFeature([OtherLinksEffects]),
    ToastNotificationsModule,
  ],
  declarations: [
    HomeComponent, 
    DiligenciesSectionComponent, 
    InformYourselfSectionComponent, 
    OtherLinksSectionComponent, 
    YourOpinionSectionComponent
  ],
  providers: [
    DiligenciesService,
    YourOpinionService,
    InformYourselfService,
    OtherLinksService
  ],
  exports: [HomeComponent],
})
export class HomeModule { }
