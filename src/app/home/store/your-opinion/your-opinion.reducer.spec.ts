import { yourOpinionReducer } from './your-opinion.reducer';
import { yourOpinionInitialState } from './your-opinion.state';

describe('YourOpinion Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = yourOpinionReducer(yourOpinionInitialState, action);

      expect(result).toBe(yourOpinionInitialState);
    });
  });
});
