import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { YourOpinionEffects } from './your-opinion.effects';

describe('ProjectsEffects', () => {
  let actions$: Observable<any>;
  let effects: YourOpinionEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        YourOpinionEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(YourOpinionEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
