import { YourOpinion } from '../../models/your-opinion.model';

export interface YourOpinionState {
  yourOpinion: YourOpinion[] | null;
  loading: boolean;
  error: any;
}

export const yourOpinionInitialState: YourOpinionState = {
  yourOpinion: null,
  loading: false,
  error: null
};
