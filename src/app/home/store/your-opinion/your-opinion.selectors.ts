import { createSelector, createFeatureSelector } from '@ngrx/store';
import { YourOpinionState } from './your-opinion.state';

export const getYourOpinionState = createFeatureSelector<YourOpinionState>('yourOpinion');

export const getYourOpinion = createSelector(
  getYourOpinionState,
  yourOpinion => yourOpinion.yourOpinion
);

export const getAllLoaded = createSelector(
  getYourOpinionState,
  yourOpinion => yourOpinion.loading
);

export const getError = createSelector(
  getYourOpinionState,
  yourOpinion => yourOpinion.error
)
