import { YourOpinionActions, YourOpinionActionTypes } from './your-opinion.actions';
import { yourOpinionInitialState, YourOpinionState } from './your-opinion.state';


export function yourOpinionReducer(state = yourOpinionInitialState, action: YourOpinionActions): YourOpinionState {
  
  switch (action.type) {
    
    case YourOpinionActionTypes.YOUR_OPINION_QUERY: {
      return Object.assign({}, state, {
        loading: true,
      });
    }

    case YourOpinionActionTypes.YOUR_OPINION_LOADED: {
      return Object.assign({}, state, {
        yourOpinion: action.payload.yourOpinion,
        loading: false,
      });
    }

    case YourOpinionActionTypes.YOUR_OPINION_ERROR: {
      return Object.assign({}, state, {
        loading: false,
        error: action.payload.error
      });
    }

    default:
      return state;
  }
}
