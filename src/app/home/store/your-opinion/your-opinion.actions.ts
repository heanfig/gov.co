import { Action } from '@ngrx/store';
import { YourOpinion } from '../../models/your-opinion.model';

export enum YourOpinionActionTypes {
  YOUR_OPINION_QUERY  = '[YourOpinion] YourOpinion query',
  YOUR_OPINION_ADDED  = '[YourOpinion] YourOpinion added',
  YOUR_OPINION_LOADED = '[YourOpinion] YourOpinion loaded',
  YOUR_OPINION_ERROR  = '[YourOpinion] YourOpinion error'
}

export class YourOpinionQuery implements Action {
  readonly type = YourOpinionActionTypes.YOUR_OPINION_QUERY;
}

export class YourOpinionLoaded implements Action {
  readonly type = YourOpinionActionTypes.YOUR_OPINION_LOADED;

  constructor(public payload: { yourOpinion: YourOpinion[] }) {}
}

export class YourOpinionAdded implements Action {
  readonly type = YourOpinionActionTypes.YOUR_OPINION_ADDED;

  constructor(public payload: { opinion: string, yourOpinion: YourOpinion }) {}
}

export class YourOpinionError implements Action {
  readonly type = YourOpinionActionTypes.YOUR_OPINION_ERROR;

  constructor(public payload: { error: any }) {}
}

export type YourOpinionActions =
  | YourOpinionQuery
  | YourOpinionAdded
  | YourOpinionLoaded
  | YourOpinionError;
