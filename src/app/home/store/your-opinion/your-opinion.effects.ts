import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { YourOpinionActionTypes } from './your-opinion.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as fromYouOpinion from './your-opinion.actions';
import { YourOpinionService } from '../../services/your-opinion.service';
import { YourOpinion } from '../../models/your-opinion.model';

@Injectable()
export class YourOpinionEffects {

  /**
   *Creates an instance of YourOpinionEffects.
   * @param {Actions} actions$
   * @param {YourOpinionService} yourOpinionService
   * @memberof YourOpinionEffects
   */
  constructor(
    private actions$: Actions,
    private yourOpinionService: YourOpinionService) {}

  @Effect()
  query$ = this.actions$.pipe(
    ofType(YourOpinionActionTypes.YOUR_OPINION_QUERY),
    switchMap(() => {
      return this.yourOpinionService.get()
      .pipe(
        map((data: any) => {
          const opinionData: YourOpinion[] = data.map((res: any) => {
            const key = res.payload.key;
            const opinion: YourOpinion = res.payload.val();
            return {
              key: key || null,
              state: opinion.state || null,
              title: opinion.title || null,
              author: opinion.author || null,
              description: opinion.description || null,
              participations: opinion.participations || [],
              featured: opinion.featured || null,
            };
          });
          return (new fromYouOpinion.YourOpinionLoaded({ yourOpinion: opinionData }));
        }),
        catchError(error => of(new fromYouOpinion.YourOpinionError({ error })))
      );
    }),
  );

  @Effect({ dispatch: false })
  added$ = this.actions$.pipe(
    ofType(YourOpinionActionTypes.YOUR_OPINION_ADDED),
    map((action: fromYouOpinion.YourOpinionAdded) => action.payload),
    switchMap((payload: any) => this.yourOpinionService.add( payload.opinion, payload.yourOpinion ))
  );

}
