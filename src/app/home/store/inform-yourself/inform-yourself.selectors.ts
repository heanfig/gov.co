import { createSelector, createFeatureSelector } from '@ngrx/store';
import { InformYourselfState } from './inform-yourself.state';

export const getInformYourselfState = createFeatureSelector<InformYourselfState>('informYourself');

export const getInformYourself = createSelector(
  getInformYourselfState,
  informYourself => informYourself.informYourself
);

export const getAllLoaded = createSelector(
  getInformYourselfState,
  informYourself => informYourself.loading
);

export const getError = createSelector(
  getInformYourselfState,
  informYourself => informYourself.error
)
