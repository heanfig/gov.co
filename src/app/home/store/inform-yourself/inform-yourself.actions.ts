import { Action } from '@ngrx/store';
import { InformYourself } from '../../models/inform-yourself.model';

/**
 * Action types for Inform Yourself
 * @export
 * @enum {number}
 */
export enum InformYourselfActionTypes {
  INFORM_YOURSELF_QUERY = '[InformYourself] InformYourself query',
  INFORM_YOURSELF_LOADED = '[InformYourself] InformYourself loaded',
  INFORM_YOURSELF_ERROR = '[InformYourself] InformYourself error'
}

/**
 * Action Type Query for Inform
 * @export
 * @class InformYourselfQuery
 * @implements {Action}
 */
export class InformYourselfQuery implements Action {
  readonly type = InformYourselfActionTypes.INFORM_YOURSELF_QUERY;
}

/**
 * Action type loaded for Inform
 * @export
 * @class InformYourselfLoaded
 * @implements {Action}
 */
export class InformYourselfLoaded implements Action {
  readonly type = InformYourselfActionTypes.INFORM_YOURSELF_LOADED;

  constructor(public payload: { informYourself: InformYourself[] }) {}
}

/**
 * Action types error for inform
 * @export
 * @class InformYourselfError
 * @implements {Action}
 */
export class InformYourselfError implements Action {
  readonly type = InformYourselfActionTypes.INFORM_YOURSELF_ERROR;

  constructor(public payload: { error: any }) {}
}

export type InformYourselfActions =
  | InformYourselfQuery
  | InformYourselfLoaded
  | InformYourselfError;
