import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { InformYourselfEffects } from './inform-yourself.effects';

describe('ProjectsEffects', () => {
  let actions$: Observable<any>;
  let effects: InformYourselfEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        InformYourselfEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(InformYourselfEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
