import { informYourselfReducer } from './inform-yourself.reducer';
import { informYourselfInitialState } from './inform-yourself.state';

describe('InformYourself Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = informYourselfReducer(informYourselfInitialState, action);

      expect(result).toBe(informYourselfInitialState);
    });
  });
});
