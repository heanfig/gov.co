import { InformYourselfActions, InformYourselfActionTypes } from './inform-yourself.actions';
import { informYourselfInitialState, InformYourselfState } from './inform-yourself.state';


export function informYourselfReducer(state = informYourselfInitialState, action: InformYourselfActions): InformYourselfState {
  
  switch (action.type) {
    
    case InformYourselfActionTypes.INFORM_YOURSELF_QUERY: {
      return Object.assign({}, state, {
        loading: true,
      });
    }

    case InformYourselfActionTypes.INFORM_YOURSELF_LOADED: {
      return Object.assign({}, state, {
        informYourself: action.payload.informYourself,
        loading: false,
      });
    }

    case InformYourselfActionTypes.INFORM_YOURSELF_ERROR: {
      return Object.assign({}, state, {
        loading: false,
        error: action.payload.error
      });
    }

    default:
      return state;
  }
}
