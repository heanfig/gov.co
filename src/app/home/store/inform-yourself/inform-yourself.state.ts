import { InformYourself } from '../../models/inform-yourself.model';

export interface InformYourselfState {
  informYourself: InformYourself[] | null;
  loading: boolean;
  error: any;
}

export const informYourselfInitialState: InformYourselfState = {
  informYourself: null,
  loading: false,
  error: null
};
