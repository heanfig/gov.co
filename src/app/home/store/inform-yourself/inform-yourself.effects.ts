import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { InformYourselfActionTypes } from './inform-yourself.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { InformYourself } from '../../models/inform-yourself.model';
import { of } from 'rxjs';
import { InformYourselfService } from '../../services/inform-yourself.service';

import * as fromInformYourself from './inform-yourself.actions';

@Injectable()
export class InformYourselfEffects {

  constructor(
    private actions$: Actions,
    private informYourselfService: InformYourselfService) {}

  @Effect()
  query$ = this.actions$.pipe(
    ofType(InformYourselfActionTypes.INFORM_YOURSELF_QUERY),
    switchMap(() => {
      return this.informYourselfService.get()
      .pipe(
        map((data: any) => {
          const informYourselfData: InformYourself[] = data.map((res: any) => {
            const key = res.payload.key;
            const informItem: InformYourself = res.payload.val();
            return {
              key: key || null,
              date: informItem.date || null,
              title: informItem.title || null,
              imageUrl: informItem.imageUrl || null,
              size: informItem.size || null,
            };
          });
          return (new fromInformYourself.InformYourselfLoaded({ informYourself: informYourselfData }));
        }),
        catchError(error => of(new fromInformYourself.InformYourselfError({ error })))
      );
    }),
  );

}
