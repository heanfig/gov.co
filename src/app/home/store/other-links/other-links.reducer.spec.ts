import { otherLinksReducer } from './other-links.reducer';
import { otherLinksInitialState } from './other-links.state';

describe('OtherLinks Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = otherLinksReducer(otherLinksInitialState, action);

      expect(result).toBe(otherLinksInitialState);
    });
  });
});
