import { OtherLink } from '../../models/other-links.model';

export interface OtherLinkState {
  otherLinks: OtherLink[] | null;
  loading: boolean;
  error: any;
}

export const otherLinksInitialState: OtherLinkState = {
  otherLinks: null,
  loading: false,
  error: null
};
