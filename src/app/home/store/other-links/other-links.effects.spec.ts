import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { OtherLinksEffects } from './other-links.effects';

describe('OtherLinksEffects', () => {
  let actions$: Observable<any>;
  let effects: OtherLinksEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OtherLinksEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(OtherLinksEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
