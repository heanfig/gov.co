import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { OtherLinksActionTypes } from './other-links.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { OtherLink } from '../../models/other-links.model';
import { of } from 'rxjs';
import { OtherLinksService } from '../../services/other-links.service';

import * as fromOtherLinks from './other-links.actions';

@Injectable()
export class OtherLinksEffects {

  constructor(
    private actions$: Actions,
    private otherLinksService: OtherLinksService) {}

  @Effect()
  query$ = this.actions$.pipe(
    ofType(OtherLinksActionTypes.OTHER_LINKS_QUERY),
    switchMap(() => {
      return this.otherLinksService.get()
      .pipe(
        map((data: any) => {
          const otherLinksData: OtherLink[] = data.map((res: any) => {
            const key = res.payload.key;
            const diligency: OtherLink = res.payload.val();
            return {
              key: key || null,
              title: diligency.title || null,
              imageUrl: diligency.imageUrl || null,
              callToActionText: diligency.callToActionText || null,
              excerpt: diligency.excerpt || null,
              link: diligency.link || null,
            };
          });
          return (new fromOtherLinks.OtherLinksLoaded({ otherLinks: otherLinksData }));
        }),
        catchError(error => of(new fromOtherLinks.OtherLinksError({ error })))
      );
    }),
  );

}
