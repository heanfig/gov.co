import { createSelector, createFeatureSelector } from '@ngrx/store';
import { OtherLinkState } from './other-links.state';

export const getOtherLinksState = createFeatureSelector<OtherLinkState>('otherLinks');

export const getOtherLinks = createSelector(
  getOtherLinksState,
  otherLinks => otherLinks.otherLinks
);

export const getAllLoaded = createSelector(
  getOtherLinksState,
  otherLinks => otherLinks.loading
);

export const getError = createSelector(
  getOtherLinksState,
  otherLinks => otherLinks.error
)
