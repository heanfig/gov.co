import { Action } from '@ngrx/store';
import { OtherLink } from '../../models/other-links.model';

export enum OtherLinksActionTypes {
  OTHER_LINKS_QUERY = '[OtherLinks] OtherLinks query',
  OTHER_LINKS_LOADED = '[OtherLinks] OtherLinks loaded',
  OTHER_LINKS_ERROR = '[OtherLinks] OtherLinks error'
}

export class OtherLinksQuery implements Action {
  readonly type = OtherLinksActionTypes.OTHER_LINKS_QUERY;
}

export class OtherLinksLoaded implements Action {
  readonly type = OtherLinksActionTypes.OTHER_LINKS_LOADED;

  constructor(public payload: { otherLinks: OtherLink[] }) {}
}

export class OtherLinksError implements Action {
  readonly type = OtherLinksActionTypes.OTHER_LINKS_ERROR;

  constructor(public payload: { error: any }) {}
}

export type OtherLinksActions =
  | OtherLinksQuery
  | OtherLinksLoaded
  | OtherLinksError;
