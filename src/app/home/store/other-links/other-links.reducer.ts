import { OtherLinksActions, OtherLinksActionTypes } from './other-links.actions';
import { otherLinksInitialState, OtherLinkState } from './other-links.state';

export function otherLinksReducer(state = otherLinksInitialState, action: OtherLinksActions): OtherLinkState {
  
  switch (action.type) {
    
    case OtherLinksActionTypes.OTHER_LINKS_QUERY: {
      return Object.assign({}, state, {
        loading: true,
      });
    }

    case OtherLinksActionTypes.OTHER_LINKS_LOADED: {
      return Object.assign({}, state, {
        otherLinks: action.payload.otherLinks,
        loading: false,
      });
    }

    case OtherLinksActionTypes.OTHER_LINKS_ERROR: {
      return Object.assign({}, state, {
        loading: false,
        error: action.payload.error
      });
    }

    default:
      return state;
  }
}
