import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DiligenciesActionTypes } from './diligencies.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { Diligency } from '../../models/diligency.model';
import { of } from 'rxjs';
import { DiligenciesService } from '../../services/diligencies.service';
import * as fromDiligencies from './diligencies.actions';

@Injectable()
export class DiligenciesEffects {

  constructor(
    private actions$: Actions,
    private diligenciesService: DiligenciesService) {}
  
  /**
   * Effect to get All diligencies from service
   * then map data and initialize
   * @memberof DiligenciesEffects
   */
  @Effect()
  query$ = this.actions$.pipe(
    ofType(DiligenciesActionTypes.DILIGENCIES_QUERY),
    switchMap(() => {
      return this.diligenciesService.get()
      .pipe(
        map((data: any) => {
          const diligenciesData: Diligency[] = data.map((res: any) => {
            const key = res.payload.key;
            const diligency: Diligency = res.payload.val();
            return {
              key: key || null,
              title: diligency.title || null,
              author: diligency.author || null,
              availableOnLine: diligency.availableOnLine || null,
              diligencyWithCost: diligency.diligencyWithCost || null,
              featured: diligency.featured || null
            };
          });
          return (new fromDiligencies.DiligenciesLoaded({ diligencies: diligenciesData }));
        }),
        catchError(error => of(new fromDiligencies.DiligenciesError({ error })))
      );
    }),
  );

}
