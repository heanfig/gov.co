import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DiligencyState } from './diligencies.state';

export const getDiligenciesState = createFeatureSelector<DiligencyState>('diligencies');

/**
 * Selector to get all diligencies
 */
export const getDiligencies = createSelector(
  getDiligenciesState,
  diligencies => diligencies.diligencies
);

/**
 * Selector to get loading
 */
export const getAllLoaded = createSelector(
  getDiligenciesState,
  diligencies => diligencies.loading
);

/**
 * Selector to get if has error
 */
export const getError = createSelector(
  getDiligenciesState,
  diligencies => diligencies.error
)
