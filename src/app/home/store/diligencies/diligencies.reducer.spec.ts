import { diligenciesReducer } from './diligencies.reducer';
import { diligenciesInitialState } from './diligencies.state';

describe('Diligencies Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = diligenciesReducer(diligenciesInitialState, action);

      expect(result).toBe(diligenciesInitialState);
    });
  });
});
