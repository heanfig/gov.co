import { Action } from '@ngrx/store';
import { Diligency } from '../../models/diligency.model';

/**
 * Diligencies ActionsTypes for request
 * @export
 * @enum {number}
 */
export enum DiligenciesActionTypes {
  DILIGENCIES_QUERY = '[Diligencies] Diligencies query',
  DILIGENCIES_LOADED = '[Diligencies] Diligencies loaded',
  DILIGENCIES_ERROR = '[Diligencies] Diligencies error'
}

/**
 * Query Action for Get Data
 * @export
 * @class DiligenciesQuery
 * @implements {Action}
 */
export class DiligenciesQuery implements Action {
  readonly type = DiligenciesActionTypes.DILIGENCIES_QUERY;
}

/**
 * Query action for loaded data
 * @export
 * @class DiligenciesLoaded
 * @implements {Action}
 */
export class DiligenciesLoaded implements Action {
  readonly type = DiligenciesActionTypes.DILIGENCIES_LOADED;

  constructor(public payload: { diligencies: Diligency[] }) {}
}

/**
 * Query action for error state
 * @export
 * @class DiligenciesError
 * @implements {Action}
 */
export class DiligenciesError implements Action {
  readonly type = DiligenciesActionTypes.DILIGENCIES_ERROR;

  constructor(public payload: { error: any }) {}
}

export type DiligenciesActions =
  | DiligenciesQuery
  | DiligenciesLoaded
  | DiligenciesError;
