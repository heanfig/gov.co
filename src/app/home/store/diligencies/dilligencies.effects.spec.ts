import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { DiligenciesEffects } from './diligencies.effects';

describe('ProjectsEffects', () => {
  let actions$: Observable<any>;
  let effects: DiligenciesEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DiligenciesEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(DiligenciesEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
