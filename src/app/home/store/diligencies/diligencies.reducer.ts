import { DiligenciesActions, DiligenciesActionTypes } from './diligencies.actions';
import { diligenciesInitialState, DiligencyState } from './diligencies.state';

/**
 * get Reducer or diligencies from state
 * if action is n point reducer to specific action
 * @export
 * @param {*} [state=diligenciesInitialState]
 * @param {DiligenciesActions} action
 * @returns {DiligencyState}
 */
export function diligenciesReducer(state = diligenciesInitialState, action: DiligenciesActions): DiligencyState {
  
  switch (action.type) {
    
    case DiligenciesActionTypes.DILIGENCIES_QUERY: {
      return Object.assign({}, state, {
        loading: true,
      });
    }

    case DiligenciesActionTypes.DILIGENCIES_LOADED: {
      return Object.assign({}, state, {
        diligencies: action.payload.diligencies,
        loading: false,
      });
    }

    case DiligenciesActionTypes.DILIGENCIES_ERROR: {
      return Object.assign({}, state, {
        loading: false,
        error: action.payload.error
      });
    }

    default:
      return state;
  }
}
