import { Diligency } from '../../models/diligency.model';

/**
 * Diligency states model
 * @export
 * @interface DiligencyState
 */
export interface DiligencyState {
  diligencies: Diligency[] | null;
  loading: boolean;
  error: any;
}

export const diligenciesInitialState: DiligencyState = {
  diligencies: null,
  loading: false,
  error: null
};
