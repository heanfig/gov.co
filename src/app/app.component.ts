import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit {

  /**
   * Creates an instance of AppComponent.
   * @param {Store<AppState>} store
   * @memberof AppComponent
   */
  constructor() {}

  ngOnInit() {
  }

}
