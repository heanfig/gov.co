import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { YourOpinion } from 'src/app/home/models/your-opinion.model';

@Component({
  selector: 'app-your-opinion-form',
  templateUrl: './your-opinion-form.component.html',
  styleUrls: ['./your-opinion-form.component.scss']
})
export class YourOpinionFormComponent implements OnInit {

  @Input() charslimit: number;
  @Output() onSubmitted = new EventEmitter<any>();
  @Input() youropinion: Observable<YourOpinion[]>;

  public participationForm: FormGroup; 
  public currentOpinionContext: YourOpinion;

  /**
   * get FormControlTextContent
   * @readonly
   * @type {FormControl}
   * @memberof YourOpinionFormComponent
   */
  get textContent(): AbstractControl | null { 
    return this.participationForm.get('textContent');
  }

  /**
   *Creates an instance of YourOpinionFormComponent.
   * @memberof YourOpinionFormComponent
   */
  constructor() { }

  /**
   * ngInit method
   * create instance of participation form and set 
   * Validators using AbstractControl
   * @memberof YourOpinionFormComponent
   */
  ngOnInit() : void {

    this.participationForm = new FormGroup({
      textContent: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(this.charslimit)
      ])
    });

    this.getCurrentOpinion();
  }

  /**
   * get Opinion from list and return only
   * featured opinion and one item
   * @memberof YourOpinionFormComponent
   */
  getCurrentOpinion(){
    this.youropinion.pipe(
      filter( opinion => opinion !== null ),
      map( items => items!.filter(opinion => opinion.featured) ),
      map( items => items.slice(0, 1) ),
    ).subscribe((yourOpinion) => {
      if(yourOpinion.length){
        const [opinion] = yourOpinion;
        this.currentOpinionContext = opinion;
      }
    });
  }

  /**
   * Send payload form
   * @memberof YourOpinionFormComponent
   */
  onSubmit() {
    this.onSubmitted.emit( {
      opinion: this.textContent!.value,
      yourOpinion: this.currentOpinionContext
    } );
    this.participationForm.reset();
  }

}
