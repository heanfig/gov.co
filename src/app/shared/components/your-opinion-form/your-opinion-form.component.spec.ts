import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourOpinionFormComponent } from './your-opinion-form.component';

describe('YourOpinionFormComponent', () => {
  let component: YourOpinionFormComponent;
  let fixture: ComponentFixture<YourOpinionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourOpinionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourOpinionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
