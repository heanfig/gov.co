import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourOpinionListComponent } from './your-opinion-list.component';

describe('YourOpinionListComponent', () => {
  let component: YourOpinionListComponent;
  let fixture: ComponentFixture<YourOpinionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourOpinionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourOpinionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
