import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { YourOpinion } from 'src/app/home/models/your-opinion.model';

@Component({
  selector: 'app-your-opinion-list',
  templateUrl: './your-opinion-list.component.html',
  styleUrls: ['./your-opinion-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class YourOpinionListComponent implements OnInit {

  @Input() youropinion: YourOpinion[];

  /**
   *Creates an instance of YourOpinionListComponent.
   * @memberof YourOpinionListComponent
   */
  constructor() { }

  /**
   * ngOnInit Method
   * @memberof YourOpinionListComponent
   */
  ngOnInit() {
  }

  /**
   * track by for iterable loops
   * @param {*} index
   * @returns
   * @memberof YourOpinionListComponent
   */
  trackByFunction(index: any) {
    return index.key;
  }

}
