import { Component, Input, OnInit } from '@angular/core';
import { InformYourself } from 'src/app/home/models/inform-yourself.model';

@Component({
  selector: 'app-inform-yourself',
  templateUrl: './inform-yourself.component.html',
  styleUrls: ['./inform-yourself.component.scss']
})
export class InformYourselfComponent implements OnInit {

  @Input() item: InformYourself;
  
  /**
   * Creates an instance of InformYourselfComponent.
   * @memberof InformYourselfComponent
   */
  constructor() { }

  /**
   * ngOnInit method
   * @memberof InformYourselfComponent
   */
  ngOnInit() {
  }

}
