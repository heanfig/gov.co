import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformYourselfComponent } from './inform-yourself.component';

describe('InformYourselfComponent', () => {
  let component: InformYourselfComponent;
  let fixture: ComponentFixture<InformYourselfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformYourselfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformYourselfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
