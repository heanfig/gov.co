import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map  } from 'rxjs/operators';
import { Diligency } from 'src/app/home/models/diligency.model';

@Component({
  selector: 'app-diligencies-list',
  templateUrl: './diligencies-list.component.html',
  styleUrls: ['./diligencies-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DiligenciesListComponent implements OnInit {

  @Input() diligencies: Observable<Diligency[]>;
  @Input() layout: string;
  @Input() featured: boolean;
  @Input() limit: number;

  public diligenciesFeatured: Observable<Diligency[]>; 

  /**
   *Creates an instance of DiligenciesListComponent.
   * @memberof DiligenciesListComponent
   */
  constructor() { }

  /**
   * ngOnInit method
   * @memberof DiligenciesListComponent
   */
  ngOnInit() : void {
    this.getDiligencies();
  }

  /**
   * Get Diligencies and filter by key
   * limit key
   * @memberof DiligenciesListComponent
   */
  getDiligencies(): void {
    this.diligencies = this.diligencies.pipe(
      filter( diligencies => diligencies !== null ),
      map( items => items.slice(0, this.limit) )
    );
    this.diligencies.subscribe(() => {
      this.getFeaturedDiligencies();
    });
  }

  /**
   * Filter Diligencies("Tramites") by feature
   * featured key in json data
   * @memberof DiligenciesListComponent
   */
  getFeaturedDiligencies(): void {
    this.diligenciesFeatured = this.diligencies.pipe(
      filter( diligencies => diligencies !== null ),
      map( items => items!.filter(diligency => diligency.featured) ),
    );
  }

  /**
   * Track by indexes
   * @param {*} index
   * @returns {any} item
   * @memberof DiligenciesListComponent
   */
  trackByFunction(index: any) {
    return index;
  }

}
