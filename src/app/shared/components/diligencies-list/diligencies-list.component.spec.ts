import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiligenciesListComponent } from './diligencies-list.component';

describe('DiligenciesListComponent', () => {
  let component: DiligenciesListComponent;
  let fixture: ComponentFixture<DiligenciesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiligenciesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiligenciesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
