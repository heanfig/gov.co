import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { OtherLink } from 'src/app/home/models/other-links.model';

@Component({
  selector: 'app-other-links-list',
  templateUrl: './other-links-list.component.html',
  styleUrls: ['./other-links-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OtherLinksListComponent implements OnInit {

  @Input() otherlinks: OtherLink[];
  
  constructor() { }

  ngOnInit() {
  }

}
