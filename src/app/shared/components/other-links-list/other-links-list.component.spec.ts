import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherLinksListComponent } from './other-links-list.component';

describe('OtherLinksListComponent', () => {
  let component: OtherLinksListComponent;
  let fixture: ComponentFixture<OtherLinksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherLinksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherLinksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
