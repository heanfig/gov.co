import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-cookies-message',
  templateUrl: './cookies-message.component.html',
  styleUrls: ['./cookies-message.component.scss']
})
export class CookiesMessageComponent implements OnInit {

  public isVisible: boolean = true;
  public opinionConsentFirstTimeKey = 'opinionConsentFirstTime';
  public opinionConsentKey = 'opinionConsent';

  /**
   *Creates an instance of CookiesMessageComponent.
   * @param {CookieService} cookieService
   * @memberof CookiesMessageComponent
   */
  constructor(protected cookieService: CookieService) { }

  /**
   * @memberof CookiesMessageComponent
   */
  ngOnInit() {
    const isVisibleCookiePane = this.cookieService.get(this.opinionConsentFirstTimeKey);
    this.isVisible = !isVisibleCookiePane ? true : false;
  }

  /**
   * Close Popup Message on click 
   * @memberof CookiesMessageComponent
   */
  closeMessage(): void {
    this.cookieService.set(this.opinionConsentFirstTimeKey,'1');
    this.isVisible = false;
  }

}
