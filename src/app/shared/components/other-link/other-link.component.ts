import { Component, Input, OnInit } from '@angular/core';
import { OtherLink } from 'src/app/home/models/other-links.model';

@Component({
  selector: 'app-other-link',
  templateUrl: './other-link.component.html',
  styleUrls: ['./other-link.component.scss']
})
export class OtherLinkComponent implements OnInit {

  @Input() otherlink: OtherLink;
  
  constructor() { }

  ngOnInit() {
  }

}
