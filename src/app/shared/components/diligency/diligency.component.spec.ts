import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiligencyComponent } from './diligency.component';

describe('DiligencyComponent', () => {
  let component: DiligencyComponent;
  let fixture: ComponentFixture<DiligencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiligencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiligencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
