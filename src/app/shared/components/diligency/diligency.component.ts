import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Diligency } from 'src/app/home/models/diligency.model';

@Component({
  selector: 'app-diligency',
  templateUrl: './diligency.component.html',
  styleUrls: ['./diligency.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiligencyComponent implements OnInit {

  @Input() diligency: Diligency;
  @Input() layout: string;

  /**
   *Creates an instance of DiligencyComponent.
   * @memberof DiligencyComponent
   */
  constructor() { }
  
  /**
   * ngInit method
   * @memberof DiligencyComponent
   */

  /**
   * ngInit method
   * @memberof DiligencyComponent
   */
  ngOnInit() {
  }

}
