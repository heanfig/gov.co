import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformYourselfListComponent } from './inform-yourself-list.component';

describe('InformYourselfListComponent', () => {
  let component: InformYourselfListComponent;
  let fixture: ComponentFixture<InformYourselfListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformYourselfListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformYourselfListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
