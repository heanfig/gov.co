import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { InformYourself } from 'src/app/home/models/inform-yourself.model';

@Component({
  selector: 'app-inform-yourself-list',
  templateUrl: './inform-yourself-list.component.html',
  styleUrls: ['./inform-yourself-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InformYourselfListComponent implements OnInit {
  
  @Input() data: InformYourself[];
  @Input() limit: number;

  constructor() { }

  ngOnInit() {
  }

}
