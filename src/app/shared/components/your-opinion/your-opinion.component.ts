import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { YourOpinion } from 'src/app/home/models/your-opinion.model';

@Component({
  selector: 'app-your-opinion',
  templateUrl: './your-opinion.component.html',
  styleUrls: ['./your-opinion.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class YourOpinionComponent implements OnInit {

  @Input() opinion: YourOpinion;

  /**
   * Creates an instance of YourOpinionComponent.
   * @memberof YourOpinionComponent
   */
  constructor() { }

  /**
   * ngOnInit Method
   * @memberof YourOpinionComponent
   */
  ngOnInit() {
  }

  /**
   * Get class Name of state 
   * @param {string} state
   * @returns {mixed} object contains the state converted
   * @memberof YourOpinionComponent
   */
  resolveState(state: string) {
    let className = '';
    let label = '';
    switch(state){
      case 'LAST_DAY':
        className = 'last-day';
        label = 'Falta un día';
      break;
      case 'ACTIVE':
        className = 'active';
        label = 'Activo!';
      break;
      case 'FINISHED':
        className = 'finished';
        label = 'Conoce los resultados';
      break;
    }
    return { className, label };
  }

  /**
   * get count of participants based on current Participation
   * @param {*} participations
   * @returns
   * @memberof YourOpinionComponent
   */
  getParticipants(participations: any){
    return Object.values(participations || []).length;
  }

}
