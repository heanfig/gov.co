import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ButtonsModule,
  InputsModule,
  CardsModule,
  InputUtilitiesModule,
  IconsModule,
  CarouselModule
} from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeroComponent } from './components/hero/hero.component';
import { DiligenciesListComponent } from './components/diligencies-list/diligencies-list.component';
import { DiligencyComponent } from './components/diligency/diligency.component';
import { YourOpinionListComponent } from './components/your-opinion-list/your-opinion-list.component';
import { YourOpinionComponent } from './components/your-opinion/your-opinion.component';
import { OtherLinksListComponent } from './components/other-links-list/other-links-list.component';
import { OtherLinkComponent } from './components/other-link/other-link.component';
import { InformYourselfListComponent } from './components/inform-yourself-list/inform-yourself-list.component';
import { InformYourselfComponent } from './components/inform-yourself/inform-yourself.component';
import { YourOpinionFormComponent } from './components/your-opinion-form/your-opinion-form.component';
import { CookiesMessageComponent } from './components/cookies-message/cookies-message.component';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    HeroComponent,
    DiligenciesListComponent,
    DiligencyComponent,
    YourOpinionListComponent,
    YourOpinionComponent,
    OtherLinksListComponent,
    OtherLinkComponent,
    InformYourselfListComponent,
    InformYourselfComponent,
    YourOpinionFormComponent,
    CookiesMessageComponent,
  ],
  imports: [
    CommonModule,
    InputsModule,
    InputUtilitiesModule,
    IconsModule,
    FormsModule,
    ButtonsModule,
    CardsModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HeroComponent, 
    DiligenciesListComponent, 
    DiligencyComponent,
    YourOpinionListComponent, 
    YourOpinionComponent,
    CarouselModule,
    OtherLinksListComponent,
    OtherLinkComponent,
    InformYourselfListComponent,
    InformYourselfComponent,
    YourOpinionFormComponent,
    CookiesMessageComponent,
  ],
  providers: [
    CookieService
  ],
  entryComponents: [
    HeroComponent
  ]
})
export class SharedModule {}
