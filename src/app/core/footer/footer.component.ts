import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  
  /**
   * Creates an instance of FooterComponent.
   * @memberof FooterComponent
   */
  constructor() { }

  /**
   * ngOnInit call
   * @memberof FooterComponent
   */
  ngOnInit() {
  }

}
