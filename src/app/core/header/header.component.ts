import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  public activeMenu: string;

  /**
   *Creates an instance of HeaderComponent.
   * @memberof HeaderComponent
   */
  constructor() { }

  /**
   * ngOnInit Call
   * @memberof HeaderComponent
   */
  ngOnInit() : void {
  }

  /**
   * Add smoth scroll on nav click in hashbang clicks
   * @param {string} elementView
   * @memberof HeaderComponent
   */
  setActiveElement( elementView: string ) : void {
    this.activeMenu = elementView;
    const element = document.querySelector(`#${elementView}`);
    if (element) element.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

}
